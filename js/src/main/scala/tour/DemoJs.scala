package tour

import com.raquo.laminar.api.L._
import org.scalajs.dom
import org.scalajs.dom.document

object DemoJs {

  def main(args: Array[String]): Unit = {
    document.addEventListener(
      "DOMContentLoaded",
      { (_: dom.Event) =>
        setupUI()
      }
    )
  }

  def setupUI(): Unit = {
    val body = dom.document.querySelector("body")
    body.innerHTML = ""

    val main = MainPage

    render(
      body,
      div(main.render)
    )
  }

}
