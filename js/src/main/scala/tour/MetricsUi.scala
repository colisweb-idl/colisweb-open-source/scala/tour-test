package tour

import com.raquo.laminar.api.L._
import tour.domain.SolutionMetrics

final case class MetricsUi(metrics: SolutionMetrics) {

  def render: HtmlElement =
    div(
      cls := "grid grid-cols-2",
      span(f"${metrics.deliveriesCount} livraisons"),
      span(f"Distance totale ${metrics.lengthKm}%.1f km"),
      span(f"Durée totale ${formatHours(metrics.durationHours)}"),
      span(f"Coût total ${metrics.cost}%.2f €"),
      span(f"Coût par livraison ${metrics.averageCostPerStep}%.2f €"),
      span(f"Prix total ${metrics.price}%.2f €"),
      span(
        f"Marge ${metrics.marginEuros}%.2f € (${metrics.marginRate * 100}%.1f %%)"
      ),
      span(
        f"Emissions totales ${metrics.emittedCO2grams / 1_000}%.0f kg CO2"
      ),
      if (metrics.isProfitable) emptyNode
      else span(cls := "font-bold text-red-600", "Tournée non rentable")
    )

  def formatHours(timeInHours: Double): String = {
    val hours   = timeInHours.toInt
    val minutes = ((timeInHours - hours) * 60).toInt
    s"$hours:$minutes"
  }
}
