package tour

import com.raquo.laminar.api.L.svg._
import com.raquo.laminar.api.L.{HtmlElement, div, seqToModifier, textToNode}
import com.raquo.laminar.nodes.ReactiveSvgElement
import org.scalajs.dom.svg.Marker
import tour.domain.algo.Segment
import tour.domain.{Coordinates, Delivery, Solution}

final case class MapUi(solution: Solution) {
  val coordinates: Seq[Coordinates] =
    solution.path.flatMap(s => Seq(s.from, s.to)).distinct
  val (longitudes, latitudes) =
    (coordinates.map(_.longitude), coordinates.map(_.latitude))
  val (minLon, maxLon) = (longitudes.min, longitudes.max)
  val (minLat, maxLat) = (latitudes.min, latitudes.max)
  val (w, h)           = (500, 500)

  def convertLat(lat: Double): String =
    ((lat - minLat) / (maxLat - minLat) * (0.8 * w) + 0.1 * w).toInt.toString

  def convertLon(lon: Double): String =
    ((lon - minLon) / (maxLon - minLon) * (0.8 * h) + 0.1 * h).toInt.toString

  def render: HtmlElement =
    div(
      svg(
        defs(
          arrowMarker
        ),
        width  := w.toString,
        height := h.toString,
        circle(
          cx := convertLat(solution.depot.latitude),
          cy := convertLon(solution.depot.longitude),
          r  := "5"
        ),
        solution.sortedDeliverySegments.map(renderSvg)
      )
    )

  private def renderSvg(segment: (Option[Delivery], Option[Delivery])) = {
    val from     = segment._1.fold(solution.depot)(_.coordinates)
    val to       = segment._2.fold(solution.depot)(_.coordinates)
    val lengthKm = Segment(from, to).lengthKm
    val fromName = segment._1.fold("dépôt")(_.orderId)
    Seq(
      line(
        x1          := convertLat(from.latitude),
        y1          := convertLon(from.longitude),
        x2          := convertLat(to.latitude),
        y2          := convertLon(to.longitude),
        stroke      := "black",
        strokeWidth := "2",
        markerEnd   := "url(#arrow)"
      ),
      text(
        f"$lengthKm%.1f km",
        textAnchor := "middle",
        x          := convertLat((from.latitude + to.latitude) / 2),
        y          := convertLon((from.longitude + to.longitude) / 2)
      ),
      text(
        fromName,
        textAnchor := "middle",
        x          := convertLat(from.latitude),
        y          := convertLon(from.longitude)
      )
    )
  }

  //https://vanseodesign.com/web-design/svg-markers/
  private val arrowMarker: ReactiveSvgElement[Marker] = marker(
    idAttr       := "arrow",
    markerWidth  := "10",
    markerHeight := "10",
    refX         := "8",
    refY         := "3",
    orient       := "auto",
    markerUnits  := "strokeWidth",
    path(d := "M0,0 L0,6 L9,3 z")
  )
}
