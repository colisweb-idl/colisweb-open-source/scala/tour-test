package tour

import com.raquo.laminar.api.L._
import io.laminext.syntax.core.thisEvents
import tour.domain.{
  Coordinates,
  CrewParameters,
  Delivery,
  Solution,
  SolutionMetrics,
  TourProblem
}
import io.laminext.tailwind.theme.Theme
import io.laminext.tailwind.theme.DefaultTheme
import io.laminext.syntax.tailwind._

object MainPage {
  Theme.setTheme(DefaultTheme.theme)

  private val initialProblem: TourProblem  = TourProblem()
  val $problem: Var[TourProblem]           = Var(initialProblem)
  val solutions                            = new EventBus[Solution]
  val $crewParameters: Var[CrewParameters] = Var(CrewParameters())

  val metrics: Signal[SolutionMetrics] =
    (solutions.events.toSignal(
      initialProblem.initial
    ) combineWith $crewParameters.signal).map(SolutionMetrics.apply)

  val render: HtmlElement = div(
    div.card.title("Dépôt"),
    onMountInsert { implicit ctx =>
      AddressUi(zoomForDepot).render
    },
    div.card.title("Livraisons"),
    onMountInsert { implicit ctx =>
      children <-- $problem.signal
        .map(_.deliveries)
        .split(_.id)((id, _, _) =>
          div(
            DeliveryUi(zoomForDelivery(id)).render,
            button.btn.xs.fill.red(
              "X",
              thisEvents(onClick).mapToUnit --> $problem.updater[Unit]((p, _) =>
                p.removeDelivery(id)
              )
            )
          )
        )
    },
    button.btn.xs.fill.green(
      "Ajouter",
      thisEvents(onClick).mapToUnit --> $problem.updater[Unit]((p, _) =>
        p.addDelivery
      )
    ),
    CrewParametersUi($crewParameters).render,
    button.btn.xs.fill.blue(
      "Calculer",
      thisEvents(onClick).map(_ => $problem.now().solve) --> solutions
    ),
    button.btn.xs.fill.red(
      "Effacer",
      thisEvents(onClick).mapToUnit --> $problem.updater[Unit]((p, _) =>
        p.clean
      )
    ),
    div.card.title("Résultats"),
    div.card.body(
      child <-- metrics.map(m => MetricsUi(m).render),
      child <-- solutions.events.map(s => MapUi(s).render),
      table(
        children <-- solutions.events.map(solution =>
          solution.sortedDeliverySegments.map { case (from, to) =>
            def repr(maybe: Option[Delivery]) =
              maybe.fold(Seq(td("dépôt"), td(solution.depot.format)))(
                delivery =>
                  Seq(td(delivery.orderId), td(delivery.coordinates.format))
              )

            tr(repr(from), repr(to))
          }
        )
      )
    )
  )

  def zoomForDelivery(
      id: Int
  )(implicit context: MountContext[_]): Var[Delivery] =
    $problem.zoom(_.delivery(id)) { d =>
      val problem = $problem.now()
      problem.updateDelivery(d)
    }(context.owner)

  def zoomForDepot(implicit context: MountContext[_]): Var[Coordinates] =
    $problem.zoom(_.depot)(d => $problem.now().copy(depot = d))(context.owner)

}
