package tour

import com.raquo.laminar.api.L._
import tour.UiInputs.inputNumberWithLabel
import tour.domain.Coordinates

final case class AddressUi($address: Var[Coordinates]) {
  val latitude  = $address.signal.map(_.latitude.formatted("%.5f"))
  val longitude = $address.signal.map(_.longitude.formatted("%.5f"))
  val render: HtmlElement =
    span( //FIXME: quand on clique sur un label ça amène sur le dépôt
      inputNumberWithLabel($address)(
        "lat",
        "latitude",
        0.000001,
        latitude,
        _ withLatitude _
      ),
      inputNumberWithLabel($address)(
        "lon",
        "longitude",
        0.000001,
        longitude,
        _ withLongitude _
      )
    )
}
