package tour.domain

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class TourProblemSpec extends AnyFlatSpec with Matchers {

  it should "use all points" in {
    val problem = TourProblem()
    val points = problem.deliveries.map(_.coordinates) :+ problem.depot

    problem.solve.path.map(_.from) should contain theSameElementsAs points
    problem.solve.path.map(_.to) should contain theSameElementsAs points
  }
}
