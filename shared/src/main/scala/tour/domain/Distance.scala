package tour.domain

import scala.math._

object Distance {
  val earthRadiusMiles  = 3958.761
  val earthRadiusMeters = earthRadiusMiles / 0.00062137

  def haversine(p1: Coordinates, p2: Coordinates): Double = {

    val deltaLat = toRadians(p2.latitude - p1.latitude)
    val deltaLon = toRadians(p2.longitude - p1.longitude)

    val hav = pow(sin(deltaLat / 2), 2) + cos(toRadians(p1.latitude)) * cos(
      toRadians(p2.latitude)
    ) * pow(sin(deltaLon / 2), 2)
    val greatCircleDistance = 2 * atan2(sqrt(hav), sqrt(1 - hav))

    earthRadiusMeters * greatCircleDistance
  }
}
