package tour.domain

import scala.util.{Random, Try}

final case class Coordinates(latitude: Double = 0, longitude: Double = 0) {
  def withLatitude(s: String): Coordinates =
    Try(s.toDouble).fold(_ => this, l => copy(latitude = l))

  def withLongitude(s: String): Coordinates =
    Try(s.toDouble).fold(_ => this, l => copy(longitude = l))

  def +(that: Coordinates): Coordinates = Coordinates(
    latitude = this.latitude + that.latitude,
    longitude = this.longitude + that.longitude
  )

  def format: String = f"$latitude%.5f,$longitude%.5f"
}

object Coordinates {
  val TPF: Coordinates = Coordinates(50.62, 3.02)

  def random(range: Double): Coordinates =
    Coordinates(
      Random.nextDouble() * range - range / 2,
      Random.nextDouble() * range - range / 2
    )
}
