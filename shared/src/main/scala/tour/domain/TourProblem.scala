package tour.domain

import tour.domain.Coordinates._
import tour.domain.algo.{SimulatedAnnealing, VrpSolution}

final case class TourProblem(
    depot: Coordinates = TPF,
    deliveries: Seq[Delivery] = Seq.fill(3)(Delivery.random(0.5, 70) + TPF)
) {
  def clean: TourProblem = copy(deliveries = Seq(Delivery()))

  def removeDelivery(id: Int): TourProblem =
    copy(deliveries = deliveries.filterNot(_.id == id))

  def addDelivery: TourProblem =
    copy(deliveries = deliveries :+ Delivery.random(0.5, 70) + TPF)

  def delivery(id: Int): Delivery = deliveries.find(_.id == id).get

  def updateDelivery(d: Delivery): TourProblem = copy(deliveries =
    deliveries.updated(deliveries.indexWhere(_.id == d.id), d)
  )

  val initial: Solution =
    Solution(
      problem = this,
      sortedCoordinates = depot +: deliveries.map(_.coordinates)
    )

  def solve: Solution = {
    val initialVrp = VrpSolution(initial.path.map(_.from))
    val sequence = SimulatedAnnealing[VrpSolution](
      cool = 0.999f.*,
      neighbor = _.tweak,
      score = _.quality
    )
      .improve(
        initialVrp,
        10000,
        initialVrp.quality
      ) //FIXME: initial temperature must be positive
      .path
    Solution(this, sequence)
  }
}
