package tour.domain

import tour.domain.algo.Segment

final case class Solution(
    problem: TourProblem,
    sortedCoordinates: Seq[Coordinates] = Nil
) {

  val depot: Coordinates = problem.depot

  val path: Seq[Segment] = {
    val (start, end) = sortedCoordinates.span(depot.!=)
    val sequence     = end ++ start :+ depot // depot at  beginning
    //FIXME: bug si on n'ajoute pas le depot
    (sequence.init zip sequence.tail).map(Segment.apply)
  }

  val sortedDeliverySegments: Seq[(Option[Delivery], Option[Delivery])] =
    path.map(s =>
      (
        problem.deliveries.find(_.coordinates == s.from),
        problem.deliveries.find(_.coordinates == s.to)
      )
    )
}
