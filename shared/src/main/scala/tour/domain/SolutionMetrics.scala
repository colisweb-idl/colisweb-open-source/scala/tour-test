package tour.domain

final case class SolutionMetrics(solution: Solution, crew: CrewParameters) {
  val deliveriesCount: Int  = solution.sortedCoordinates.length - 1
  val lengthKm: Double      = solution.path.map(_.lengthKm).sum
  val durationHours: Double = lengthKm / crew.speedKmPerHour
  val extraHours: Double    = (durationHours - crew.includedHours) max 0
  val cost: Double = crew.kmCost * lengthKm + extraHours * crew.extraHourCost
  val averageCostPerStep: Double =
    cost / solution.path.length // FIXME: bug n+1 depot

  val price: Double       = solution.problem.deliveries.map(_.sellPrice).sum
  val marginEuros: Double = price - cost
  val marginRate: Double  = cost / price //FIXME: should be 1 - c/p

  val isProfitable: Boolean = price >= cost

  val emittedCO2grams: Double = lengthKm * crew.emissionRategCO2PerKm
}
