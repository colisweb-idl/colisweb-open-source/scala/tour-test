package tour.domain.algo

import tour.domain.Coordinates

import scala.util.Random

final case class SimulatedAnnealing[S](
    cool: Double => Double,
    neighbor: S => S,
    score: S => Double
) {

  def improve(candidate: S, iterations: Int, temperature: Double): S =
    improve(candidate, candidate, score(candidate), iterations, temperature)

  def improve(
      solution: S,
      best: S,
      bestScore: Double,
      iteration: Int,
      temperature: Double
  ): S =
    if (iteration == 0) best
    else {
      val tweaked            = neighbor(solution)
      val tweakedScore       = score(tweaked)
      val currentScore       = score(solution)
      val qualityImprovement = tweakedScore - currentScore
      val willMove = qualityImprovement > 0 || Random.nextDouble() < Math.exp(
        qualityImprovement / temperature
      )
      if (iteration % 1000 == 0)
        println(s"it $iteration score $currentScore temp $temperature")

      improve(
        if (willMove) tweaked else solution,
        if (tweakedScore > bestScore) tweaked else best,
        bestScore max tweakedScore,
        iteration - 1,
        cool(temperature)
      )

    }

}

final case class VrpSolution(path: Seq[Coordinates]) {
  val quality: Double = {
    val segments =
      (path zip path.tail :+ path.head).map((Segment.apply _).tupled)
    -segments.map(_.lengthKm).sum
  }

  def tweak: VrpSolution = {
    val points   = path
    val i        = Random.nextInt(points.size)
    val j        = Random.nextInt(points.size)
    val minIndex = i min j
    val maxIndex = i max j
    if (maxIndex - minIndex <= 1) tweak
    else {
      val start = points.take(minIndex)
      val end   = points.drop(maxIndex)
      val mid   = points.slice(minIndex, maxIndex).reverse
      VrpSolution(start ++ mid ++ end)
    }
  }
}
