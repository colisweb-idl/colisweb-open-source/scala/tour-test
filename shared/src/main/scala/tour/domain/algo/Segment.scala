package tour.domain.algo

import tour.domain.{Coordinates, Distance}

final case class Segment(from: Coordinates, to: Coordinates) {
  def lengthKm: Double = Distance.haversine(from, to) / 1000
}
