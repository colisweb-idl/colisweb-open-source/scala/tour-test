package tour.domain

import tour.domain.Delivery.lastOrderId

import scala.util.Random

final case class Delivery(
    sellPrice: Double = 0,
    coordinates: Coordinates = Coordinates(),
    orderId: String = {
      lastOrderId += 1; lastOrderId
    }.toString,
    id: Int = Random.nextInt(999999)
) {
  def +(that: Coordinates): Delivery = copy(coordinates = coordinates + that)
}

object Delivery {
  private var lastOrderId: Int = 100000

  def random(range: Double, priceRange: Double): Delivery =
    Delivery(
      sellPrice = Random.nextDouble() * priceRange,
      Coordinates.random(range)
    )
}
